package people;

import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import course.Course;

public class Student extends Person {
	/* Private Fields */
	private List<Course> takenCourses;
	private float gpa;

	/** Constructor */
	public Student(String pantherID, String fName, String lName, Date birthDate, Date startDate) {

		super(pantherID, fName, lName, birthDate, startDate);
		takenCourses = new LinkedList<Course>();
		gpa = 0;
	}

	/**
	 * Getter for takenCourses
	 * 
	 * @return takenCourses
	 */
	public List<Course> getTakenCourses() {
		return takenCourses;
	}

	/**
	 * Getter for gpa
	 * 
	 * @return gpa
	 */
	public float getGpa() {
		return gpa;
	}

	/**
	 * This methods adds the course 'c' to the list 'takenCourses'. If the course is
	 * already in the list, it does NOT make any changes.
	 * 
	 * @param c
	 *            the course to be added to the list 'takenCourses'.
	 */
	public void addCourse(Course c) {
		if (this.takenCourses.contains(c) == true) {
			System.out.println("You Failed. Repeat");
		} else {
			this.takenCourses.add(c);
			System.out.println("You almost going to Graduate");
		}

	}

	public void dropCourse(Course drop) {
		if (this.takenCourses.contains(drop)) {
			this.takenCourses.remove(drop);
			System.out.println("Dropped Course:\t" + drop);
		} else {
			System.out.println("Blopper:");
		}
	}

	public void evalutate() {
		float sum = 0;
		Random rn = new Random();
		for (int i = 0; i < takenCourses.size(); i++) {
			sum+= rn.nextInt((4-1) + 1) + 1;
		}
		this.gpa = sum / takenCourses.size();

		

	}

	@Override
	public void printInfo() {
		System.out.println("-------------STUDENT------------");
		super.printInfo();
		System.out.println("GPA:\t" + this.getGpa());
		System.out.println("Courses:\t" + Arrays.toString(takenCourses.toArray()));
		System.out.println("--------------------------------");
	}

}
